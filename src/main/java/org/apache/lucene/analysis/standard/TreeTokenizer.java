/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.lucene.analysis.standard;

import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.Map;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.util.Version;


@SuppressWarnings("deprecation")
public final class TreeTokenizer extends Tokenizer {

  private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
  private int level = 0;
  private boolean done = false;
  private int finalOffset;
  private OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
  private final PositionIncrementAttribute posAtt = addAttribute(PositionIncrementAttribute.class);
  private StringBuilder resultToken = new StringBuilder();
  private char delimiter = '\\'; //@TODO: parameterize
  private boolean addDelimiterToLastTerm = true; //@TODO: parameterize
  private int bufferSize = 512;
  private int startPosition = 0;
  private int skipped = 0;
  private boolean endDelimiter = false;
  private int charsRead = 0;
  private int skip = 0;
  private char replacement = delimiter;
 
  public TreeTokenizer() {
    
  }

  /*
   * (non-Javadoc)
   *
   * @see org.apache.lucene.analysis.TokenStream#next()
   */
  @Override
  public final boolean incrementToken() throws IOException {
    level+=1; //this is the 1-based level counter
    clearAttributes();
    String prefix_num = Integer.toString(level);
    int prefix_length = prefix_num.length()+1;
    termAtt.append( prefix_num );
    termAtt.append( delimiter );
    termAtt.append( resultToken ); //the result from the previous token
    if(level == 1){
      posAtt.setPositionIncrement(1);
    }
    else{
      posAtt.setPositionIncrement(0);
    }
    int length = prefix_length;
    boolean added = false; //???
    if( endDelimiter ){
      termAtt.append(replacement);
      length++;
      endDelimiter = false;
      added = true;
    }
    while (true) {
      int c = input.read();
      if (c >= 0) {
        charsRead++;
      } else {
        //final state
        if( skipped > skip ) {
          length += resultToken.length();
          if(addDelimiterToLastTerm){
            termAtt.append(delimiter); 
            length++;
          }
          termAtt.setLength(length);
          offsetAtt.setOffset(correctOffset(startPosition), correctOffset(startPosition + charsRead));
          if( added ){
            resultToken.setLength(0);
            resultToken.append(termAtt.buffer(), 0, length);
          }
          return added;
        }
        else{
          return false;
        }
      }
      if( !added ){
        added = true;
        skipped++;
        if( skipped > skip ){
          termAtt.append(c == delimiter ? replacement : (char)c);
          length++;
        }
        else {
          startPosition++;
        }
      }
      else {
        if( c == delimiter ){
          if( skipped > skip ){
            endDelimiter = true;
            break;
          }
          skipped++;
          if( skipped > skip ){
            termAtt.append(replacement);
            length++;
          }
          else {
            startPosition++;
          }
        }
        else {
          if( skipped > skip ){
            termAtt.append((char)c);
            length++;
          }
          else {
            startPosition++;
          }
        }
      }
    }
    length += resultToken.length();
    termAtt.setLength(length);
    offsetAtt.setOffset(correctOffset(startPosition), correctOffset(startPosition+charsRead));
    resultToken.setLength(0);
    resultToken.append(termAtt.buffer(), prefix_length, length-prefix_length);
    return true;
  }

@Override
  public final void end() throws IOException {
    super.end();
    // set final offset
    int finalOffset = correctOffset(charsRead);
    offsetAtt.setOffset(finalOffset, finalOffset);
  }

  @Override
  public void reset() throws IOException {
    super.reset();
    resultToken.setLength(0);
    charsRead = 0;
    endDelimiter = false;
    skipped = 0;
    startPosition = 0;
    level = 0;
  }
}
