package org.apache.lucene.analysis.standard;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Collections;

import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.util.Version;
import org.junit.Test;

/**
 * Testing the default word boundary characteristics for many of the punctuation
 * characters in the ASCII Unicode set
 */
public class TreeTokenizerTest {

  @Test
  public void defaultWordBoundaries_break() throws IOException {
    
    System.out.println("TEST: big 3");
    TestHelper.assertTokens(
      createTreeTokenizer("hey\\there\\man"), 
      new String[]{ "1\\hey","2\\hey\\there","3\\hey\\there\\man\\" }
    );
    System.out.println("TEST: one");
    TestHelper.assertTokens(
      createTreeTokenizer("hey"), 
      new String[]{ "1\\hey\\" }
    );
    System.out.println("TEST: case");
    TestHelper.assertTokens(
      createTreeTokenizer("Hey There"), 
      new String[]{ "1\\Hey There\\" }
    );
    System.out.println("TEST: blank lvl 1");
    TestHelper.assertTokens(
      createTreeTokenizer(""), 
      new String[]{ }
    );
    System.out.println("TEST: blank lvl 2");
    TestHelper.assertTokens(
      createTreeTokenizer("hey\\"), 
      new String[]{ "1\\hey", "2\\hey\\\\" }
    );
    System.out.println("TEST: over 10 lvls");
    TestHelper.assertTokens(
      createTreeTokenizer("a\\b\\c\\d\\e\\f\\g\\h\\i\\j\\k"), 
      new String[]{ "1\\a","2\\a\\b","3\\a\\b\\c", "4\\a\\b\\c\\d", "5\\a\\b\\c\\d\\e", "6\\a\\b\\c\\d\\e\\f", "7\\a\\b\\c\\d\\e\\f\\g", "8\\a\\b\\c\\d\\e\\f\\g\\h", "9\\a\\b\\c\\d\\e\\f\\g\\h\\i", "10\\a\\b\\c\\d\\e\\f\\g\\h\\i\\j", "11\\a\\b\\c\\d\\e\\f\\g\\h\\i\\j\\k\\" }
    );
  }

  /**
   * Create an {@link TreeTokenizer} with no character mapping
   * overrides
   */
  private static Tokenizer createTreeTokenizer(final String input) {
    Tokenizer tokenizer = new TreeTokenizer();
    tokenizer.setReader(new StringReader(input));
    return tokenizer;
  }

  private static Object[] args(int length, char chr) {
    Object[] args = new Object[length];
    Arrays.fill(args, chr);
    return args;
  }
}
