ElasticSearch Tree Tokenizer
============================

Introduction
------------

Built against specific versions.
Not all tests pass yet.

Installation
------------

In order to install (X.Y.Z is ES version)

    ./bin/plugin --install analysis-tree --url http://cdn.shooju.com/elasticsearch-analysis-tree-X.Y.Z-SNAPSHOT.jar

Development
------------

Run tests:

    mvn test

Build:

    mvn clean install


Usage
------------

  {
    "index": {
      "analysis": {
        "analyzer": {
          "my_tree_analyzer": {
            "type": "custom",
            "tokenizer": "my_tree_tokenizer"
          }
        },
        "tokenizer": {
          "my_tree_tokenizer": {
            "type": "tree"
          }
        }
      }
    }
  }

Author Information
==================
Serge Aluker
serge [at] shooju.com

License
=======

Elasticsearch Tree Tokenizer Plugin

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.